module.exports = {
    files: {
        javascripts: {
            joinTo: {
                'app.js': /^app/,
                'vendor.js': /^node_modules/
            }
        },
        stylesheets: {
            joinTo: 'app.css',
        }
    },
    modules: {
        autoRequire: {
            'app.js': ['app']
        }
    },
    paths: {
        public: './public',
        watched: ['app']
    },
    
    npm: {
        enabled: true,
        globals: {
            jQuery: 'jquery',
            $: 'jquery',
            bootstrap: 'bootstrap'
        },
        styles: {
            bootstrap: ['dist/css/bootstrap.css']
        }
    }
}
