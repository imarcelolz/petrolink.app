const SocketIO = require('socket.io');
const { DataProvider } = require("./data-provider");

class WebSocketServer {

  constructor(server) {
    this.rowNumber = 0;

    this.dataProvider = new DataProvider();

    this.server = new SocketIO(server);
    console.debug('Websocket listening on ws://localhost?');
    this.server.on('connection', (ws) => this.onConnection(ws));
  }

  onConnection(ws) {
    console.debug('socket: onConnection');

    ws.send({ message: 'Connected' });

    // send first 1000 rows to populate the chart
    const currentDate = new Date();
    for(var i = 0 ; i < 2000 ; i++){
      const message = this.dataProvider.jsonRow(this.rowNumber++);
      message.MessageDate = currentDate;

      ws.emit('data', message);
    }
    
    this.streamData(ws);
  }

  streamData(ws) {
    console.log('message received:');
    const sendMessage = () => {

      const message = this.dataProvider.jsonRow(this.rowNumber++);

      if (message == null) {
        this.rowNumber = 0;
      }

      message.MessageDate = new Date();
      ws.emit('data', message);
    };

    // I should stop this loop when client drop its connection
    setInterval(sendMessage, 1000);
  }
}

exports.WebSocketServer = WebSocketServer;
