const express = require('express');
const httpModule = require('http');
const { WebSocketServer } = require("./websocket-server");

const port = process.env.PORT || 3000;


const app = express();
const http = httpModule.createServer(app);
const wss = new WebSocketServer(http);

app.use(express.static(__dirname + '/../public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '../public/index.html');
});


http.listen(port, () => {
  console.log('Listening on http://localhost:' + port);
});


