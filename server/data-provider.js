const fs = require('fs');

class DataProvider {

  constructor() {
    this.dataBuffer = fs.readFileSync(__dirname + '/Time_HKLD.csv')
      .toString()
      .split('\n');
  }

  jsonRow(i) {
    if (this.dataBuffer.length < i) {
      return null;
    }

    const line = this.dataBuffer[i];
    const splited = line.split(',');

    return {
      Date: new Date(splited[0]),
      Value: parseFloat(splited[1], 10)
    };
  }

}
exports.DataProvider = DataProvider;
