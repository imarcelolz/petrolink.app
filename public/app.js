(function() {
  'use strict';

  var globals = typeof global === 'undefined' ? self : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};
  var aliases = {};
  var has = {}.hasOwnProperty;

  var expRe = /^\.\.?(\/|$)/;
  var expand = function(root, name) {
    var results = [], part;
    var parts = (expRe.test(name) ? root + '/' + name : name).split('/');
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function expanded(name) {
      var absolute = expand(dirname(path), name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var hot = hmr && hmr.createHot(name);
    var module = {id: name, exports: {}, hot: hot};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var expandAlias = function(name) {
    return aliases[name] ? expandAlias(aliases[name]) : name;
  };

  var _resolve = function(name, dep) {
    return expandAlias(expand(dirname(name), dep));
  };

  var require = function(name, loaderPath) {
    if (loaderPath == null) loaderPath = '/';
    var path = expandAlias(name);

    if (has.call(cache, path)) return cache[path].exports;
    if (has.call(modules, path)) return initModule(path, modules[path]);

    throw new Error("Cannot find module '" + name + "' from '" + loaderPath + "'");
  };

  require.alias = function(from, to) {
    aliases[to] = from;
  };

  var extRe = /\.[^.\/]+$/;
  var indexRe = /\/index(\.[^\/]+)?$/;
  var addExtensions = function(bundle) {
    if (extRe.test(bundle)) {
      var alias = bundle.replace(extRe, '');
      if (!has.call(aliases, alias) || aliases[alias].replace(extRe, '') === alias + '/index') {
        aliases[alias] = bundle;
      }
    }

    if (indexRe.test(bundle)) {
      var iAlias = bundle.replace(indexRe, '');
      if (!has.call(aliases, iAlias)) {
        aliases[iAlias] = bundle;
      }
    }
  };

  require.register = require.define = function(bundle, fn) {
    if (bundle && typeof bundle === 'object') {
      for (var key in bundle) {
        if (has.call(bundle, key)) {
          require.register(key, bundle[key]);
        }
      }
    } else {
      modules[bundle] = fn;
      delete cache[bundle];
      addExtensions(bundle);
    }
  };

  require.list = function() {
    var list = [];
    for (var item in modules) {
      if (has.call(modules, item)) {
        list.push(item);
      }
    }
    return list;
  };

  var hmr = globals._hmr && new globals._hmr(_resolve, require, modules, cache);
  require._cache = cache;
  require.hmr = hmr && hmr.wrap;
  require.brunch = true;
  globals.require = require;
})();

(function() {
var global = typeof window === 'undefined' ? this : window;
var process;
var __makeRelativeRequire = function(require, mappings, pref) {
  var none = {};
  var tryReq = function(name, pref) {
    var val;
    try {
      val = require(pref + '/node_modules/' + name);
      return val;
    } catch (e) {
      if (e.toString().indexOf('Cannot find module') === -1) {
        throw e;
      }

      if (pref.indexOf('node_modules') !== -1) {
        var s = pref.split('/');
        var i = s.lastIndexOf('node_modules');
        var newPref = s.slice(0, i).join('/');
        return tryReq(name, newPref);
      }
    }
    return none;
  };
  return function(name) {
    if (name in mappings) name = mappings[name];
    if (!name) return;
    if (name[0] !== '.' && pref) {
      var val = tryReq(name, pref);
      if (val !== none) return val;
    }
    return require(name);
  }
};
require.register("app.ts", function(exports, require, module) {
"use strict";
var drill_chart_1 = require("./drill-chart");
var websocket_client_1 = require("./websocket-client");
var App = (function () {
    function App() {
        var _this = this;
        this.newData = function (e) {
            if (!e) {
                return;
            }
            _this.chart.addData(e.Date, e.Value);
        };
        this.websocket = new websocket_client_1.WebSocketClient();
        this.websocket.addListner(this.newData);
        var container = document.getElementById('drilling-chart');
        this.chart = new drill_chart_1.DrillChart(container);
        this.start();
    }
    App.prototype.start = function () {
        console.debug('started called');
        this.websocket.start();
    };
    App.prototype.stop = function () {
        this.websocket.stop();
    };
    return App;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = new App;
//# sourceMappingURL=app.js.map
});

;require.register("drill-chart.ts", function(exports, require, module) {
"use strict";
var DrillChart = (function () {
    function DrillChart($container) {
        this.$container = $container;
        this.chart = am4core.create(this.$container, am4charts.XYChart);
        this.chart.data = [];
        this.chart.dateFormatter.inputDateFormat = "i";
        var dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
        var valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
        var series = this.chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "Value";
        series.dataFields.dateX = "Date";
        series.tooltipText = "{Value}";
        series.strokeWidth = 2;
        series.minBulletDistance = 15;
        series.tooltip.background.cornerRadius = 20;
        series.tooltip.background.strokeOpacity = 0;
        series.tooltip.pointerOrientation = "vertical";
        series.tooltip.label.minWidth = 40;
        series.tooltip.label.minHeight = 40;
        series.tooltip.label.textAlign = "middle";
        series.tooltip.label.textValign = "middle";
        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.circle.strokeWidth = 2;
        bullet.circle.radius = 4;
        bullet.circle.fill = am4core.color("#fff");
        var bullethover = bullet.states.create("hover");
        bullethover.properties.scale = 1.3;
        this.chart.cursor = new am4charts.XYCursor();
        this.chart.cursor.behavior = "panXY";
        this.chart.cursor.xAxis = dateAxis;
        this.chart.cursor.snapToSeries = series;
        this.chart.scrollbarX = new am4charts.XYChartScrollbar();
        this.chart.scrollbarX.series.push(series);
        this.chart.scrollbarX.parent = this.chart.bottomAxesContainer;
        this.chart.events.on("ready", function () {
            dateAxis.zoom({ start: 0.79, end: 1 });
        });
    }
    DrillChart.prototype.addData = function (date, value) {
        this.chart.addData({ Date: date, Value: value });
    };
    return DrillChart;
}());
exports.DrillChart = DrillChart;
//# sourceMappingURL=drill-chart.js.map
});

;require.register("websocket-client.ts", function(exports, require, module) {
"use strict";
var WebSocketClient = (function () {
    function WebSocketClient() {
        var _this = this;
        this.onConnected = function (e) {
            console.debug("Socket connected!!");
        };
        this.onData = function (e) {
            console.debug(e);
            console.debug("Message!!");
            if (!_this.messageCallback) {
                return;
            }
            _this.messageCallback(e);
        };
        this.socket = new io({
            autoConnect: false
        });
        //this.socket.on('connect', (d) => this.onOpen(d));
        this.socket.on('data', function (d) { return _this.onData(d); });
    }
    Object.defineProperty(WebSocketClient.prototype, "Running", {
        get: function () {
            return !this.socket || !this.socket.connected;
        },
        enumerable: true,
        configurable: true
    });
    WebSocketClient.prototype.addListner = function (callback) {
        this.messageCallback = callback;
    };
    WebSocketClient.prototype.start = function () {
        if (!this.Running) {
            return;
        }
        this.socket.connect();
    };
    WebSocketClient.prototype.stop = function () {
        if (!this.Running) {
            return;
        }
        this.socket.close();
    };
    return WebSocketClient;
}());
exports.WebSocketClient = WebSocketClient;
//# sourceMappingURL=websocket-client.js.map
});

;require.alias("process/browser.js", "process");process = require('process');require.register("___globals___", function(exports, require, module) {
  

// Auto-loaded modules from config.npm.globals.
window.jQuery = require("jquery");
window["$"] = require("jquery");
window.bootstrap = require("bootstrap");


});})();require('___globals___');

require('app');
//# sourceMappingURL=app.js.map