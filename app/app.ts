import * as bootstrap from 'bootstrap';

import { DrillChart } from './drill-chart';
import { WebSocketClient } from "./websocket-client";


class App {
	websocket: WebSocketClient;
	chart: DrillChart;

	constructor() {
		this.websocket = new WebSocketClient();
		this.websocket.addListner(this.newData);

		const container = document.getElementById('drilling-chart');
		this.chart = new DrillChart(container);
		this.start();
	}

	public start() {
		console.debug('started called');
		this.websocket.start();
	}

	public stop() {
		this.websocket.stop();
	}

	public newData = (e: any) => {

		if (!e) {
			return;
		}

		this.chart.addData(e.Date, e.Value);
	}
}

export default new App