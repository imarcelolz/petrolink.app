
export class WebSocketClient {
	socket: io;
	messageCallback: any;

	public get Running(): boolean {
		return !this.socket || !this.socket.connected;
	}

	constructor() {
		this.socket = new io({
 			autoConnect: false
		});

		//this.socket.on('connect', (d) => this.onOpen(d));
		this.socket.on('data', (d) => this.onData(d));
	}

	public addListner(callback: any) {
		this.messageCallback = callback;
	}

	public start() {
		if(!this.Running){ return ; }

		this.socket.connect();
	}

	public stop() {
		if(!this.Running){ return ; }

		this.socket.close();
	}

	private onConnected = (e: any): any => {
		console.debug("Socket connected!!");
	}

	private onData = (e: any) => {
		console.debug(e);
		console.debug("Message!!");

		if (!this.messageCallback) { return; }

		this.messageCallback(e);
	}
}
