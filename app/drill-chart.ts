
declare const am4core: any;
declare const am4charts: any;


export class DrillChart {

  private chart: any;
  
  constructor(private $container: any) {

    this.chart = am4core.create(this.$container, am4charts.XYChart);
  
		this.chart.data = [];

		this.chart.dateFormatter.inputDateFormat = "i";
		
		let dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
		let valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());

		
		let series = this.chart.series.push(new am4charts.LineSeries());
		series.dataFields.valueY = "Value";
		series.dataFields.dateX = "Date";
		series.tooltipText = "{Value}";
		series.strokeWidth = 2;
		series.minBulletDistance = 15;

		series.tooltip.background.cornerRadius = 20;
		series.tooltip.background.strokeOpacity = 0;
		series.tooltip.pointerOrientation = "vertical";
		series.tooltip.label.minWidth = 40;
		series.tooltip.label.minHeight = 40;
		series.tooltip.label.textAlign = "middle";
		series.tooltip.label.textValign = "middle";

		let bullet = series.bullets.push(new am4charts.CircleBullet());
		bullet.circle.strokeWidth = 2;
		bullet.circle.radius = 4;
		bullet.circle.fill = am4core.color("#fff");

		let bullethover = bullet.states.create("hover");
		bullethover.properties.scale = 1.3;

		this.chart.cursor = new am4charts.XYCursor();
		this.chart.cursor.behavior = "panXY";
		this.chart.cursor.xAxis = dateAxis;
		this.chart.cursor.snapToSeries = series;

		this.chart.scrollbarX = new am4charts.XYChartScrollbar();
		this.chart.scrollbarX.series.push(series);
		this.chart.scrollbarX.parent = this.chart.bottomAxesContainer;

		this.chart.events.on("ready", () => {
		  dateAxis.zoom({ start: 0.79, end: 1 });
		});
  }

  addData(date: Date, value: number) {
    this.chart.addData({ Date: date, Value: value });
  }

}
